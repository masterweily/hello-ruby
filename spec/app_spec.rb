require 'rspec'
require './app'

describe App do
  describe 'hello' do
    it { expect(App.hello).to eql 'hello' }
  end

  describe 'should fail' do
    it { expect(App.hello).to eql 'fail' }
  end
end
